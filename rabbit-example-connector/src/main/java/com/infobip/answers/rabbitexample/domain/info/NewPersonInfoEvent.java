package com.infobip.answers.rabbitexample.domain.info;

public record NewPersonInfoEvent(
    String firstName,
    String lastName,
    Long creationTimestamp
) implements InformationEvent {

    @Override
    public String routingKey() {
        return "event.info.person";
    }

}
