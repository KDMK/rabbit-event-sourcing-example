package com.infobip.answers.rabbitexample.domain.info;

public interface InformationEvent {

    String TARGET_QUEUE = "answers-information-event-queue";

    String routingKey();
}
