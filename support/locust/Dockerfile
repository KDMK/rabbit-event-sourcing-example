ARG PYTHON_VERSION=3

FROM python:${PYTHON_VERSION} as builder

ENV PYTHONUNBUFFERED 1

# build time dependencies
RUN apt-get update && \
        apt-get install -y \
        gcc \
        g++ \
        musl-dev

# build wheels instead of installing
WORKDIR /wheels
RUN pip install -U pip && \
    pip wheel locust pyzmq


FROM python:${PYTHON_VERSION}

# dependencies you need in your final image
RUN apt-get update && \
    apt-get install -y \
        # good to have bash available
        bash \
        # locustio doesn't start without it
        libzmq5

# copy built previously wheels archives
COPY --from=builder /wheels /wheels

# use archives from /weels dir
RUN pip install -U pip \
       && pip install locust -f /wheels \
       && rm -rf /wheels \
       && rm -rf /root/.cache/pip/*

# Expose locust web ui port
EXPOSE 8089

# Example locust file with simplest test
RUN mkdir /tmp/locustio
WORKDIR /tmp/locustio
COPY ./locustfile.py .
COPY ./docker-entrypoint.sh .

RUN chmod +x ./docker-entrypoint.sh

ARG RUN_ARGS=""
ENV RUN_ARGS $RUN_ARGS
ENV TERM "xterm"

#CMD ["/bin/bash", "-c", "locust", "-f", "locustfile.py", $RUN_ARGS]
ENTRYPOINT ["/tmp/locustio/docker-entrypoint.sh"]
