from locust import HttpUser, task, between

class TestRabbitProducer(HttpUser):
    wait_time = between(1, 5)

    @task
    def hello_world(self):
        self.client.post("/api/person/update/enqueue", json={"id": "1" ,"firstName": "John", "lastName": "Doe", "email": "john.doe@test.com"})
