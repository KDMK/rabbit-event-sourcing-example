#!/bin/bash

dnf update -y
dnf install -y epel-release
dnf install -y htop vim tmux

# https://github.com/rabbitmq/erlang-rpm
rpm --import https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc
rpm --import https://packagecloud.io/rabbitmq/erlang/gpgkey

# https://github.com/rabbitmq/erlang-rpm
cat > /etc/yum.repos.d/rabbitmq_erlang.repo <<EOF
[rabbitmq-rabbitmq-erlang]
name=rabbitmq-rabbitmq-erlang
baseurl=https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-erlang/rpm/el/8/\$basearch
repo_gpgcheck=1
enabled=1
gpgkey=https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-erlang/gpg.E495BB49CC4BBE5B.key
gpgcheck=1
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300
pkg_gpgcheck=1
autorefresh=1
type=rpm-md

[rabbitmq-rabbitmq-erlang-noarch]
name=rabbitmq-rabbitmq-erlang-noarch
baseurl=https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-erlang/rpm/el/8/noarch
repo_gpgcheck=1
enabled=1
gpgkey=https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-erlang/gpg.E495BB49CC4BBE5B.key
       https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc
gpgcheck=1
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300
pkg_gpgcheck=1
autorefresh=1
type=rpm-md

[rabbitmq-rabbitmq-erlang-source]
name=rabbitmq-rabbitmq-erlang-source
baseurl=https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-erlang/rpm/el/8/SRPMS
repo_gpgcheck=1
enabled=1
gpgkey=https://dl.cloudsmith.io/public/rabbitmq/rabbitmq-erlang/gpg.E495BB49CC4BBE5B.key
       https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc
gpgcheck=1
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300
pkg_gpgcheck=1
autorefresh=1
type=rpm-md
EOF

dnf update -y
dnf install --repo rabbitmq-rabbitmq-erlang -y erlang
dnf install -y socat logrotate

rpm --import https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc
rpm --import https://packagecloud.io/rabbitmq/erlang/gpgkey
rpm --import https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey

cat > /etc/yum.repos.d/rabbitmq.repo <<EOF
[rabbitmq_server]
name=rabbitmq_server
baseurl=https://packagecloud.io/rabbitmq/rabbitmq-server/el/8/\$basearch
repo_gpgcheck=1
gpgcheck=0
enabled=1
# PackageCloud's repository key and RabbitMQ package signing key
gpgkey=https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey
       https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300

[rabbitmq_server-source]
name=rabbitmq_server-source
baseurl=https://packagecloud.io/rabbitmq/rabbitmq-server/el/8/SRPMS
repo_gpgcheck=1
gpgcheck=0
enabled=1
gpgkey=https://packagecloud.io/rabbitmq/rabbitmq-server/gpgkey
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300
EOF

dnf update -y
dnf install -y --repo rabbitmq_server rabbitmq-server

# Generate same Erlang cookie
cat > /root/.erlang.cookie <<EOF
IFJCPEHJPEVVY7WOHV2KIFJCPEHJPEVVY7WOHV2K
EOF

chmod 600 /root/.erlang.cookie
cp /root/.erlang.cookie /var/lib/rabbitmq/.erlang.cookie

# Set RabbitMQ firewall exceptions
firewall-cmd --permanent --add-port=4369/tcp
firewall-cmd --permanent --add-port=5671-5672/tcp
firewall-cmd --permanent --add-port=8883/tcp
firewall-cmd --permanent --add-port=15672/tcp
firewall-cmd --permanent --add-port=25672/tcp
firewall-cmd --permanent --add-port=61613-61614/tcp

firewall-cmd --reload

# Allow SELinux to enable RabbitMQ service
setsebool -P nis_enabled 1

# Enable management console
rabbitmq-plugins enable rabbitmq_management

systemctl enable --now rabbitmq-server

rabbitmqctl add_user admin admin
rabbitmqctl set_user_tags admin administrator
rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"

# On the secondary node run following commands manually
# rabbitmqctl stop_app
# rabbitmqctl reset
# rabbitmqctl join_cluster rabbit@rabbit1
# rabbitmqctl start_app


# wget localhost:15672/cli/rabbitmqadmin
# cp rabbitmqadmin /usr/local/sbin
# rabbitmqadmin --bash-completion > /etc/bash_completion.d/rabbitmqadmin

# rabbitmqadmin declare exchange name=rabbit-example type=topic durable=true \
#   internal=false auto_delete=false

# rabbitmqadmin declare policy name=replicate-all pattern='answers-.*' \
#   apply-to=queues definition='ha-modes:all'

# rabbitmqadmin declare queue name=answers-processing-command-queue \
#   auto_delete=false durable=true  node=rabbit@rabbit1 queue_type=classic
# rabbitmqadmin declare queue name=answers-processing-event-queue \
#   auto_delete=false durable=true  node=rabbit@rabbit2 queue_type=classic
# rabbitmqadmin declare queue name=answers-information-event-queue \
#   auto_delete=false durable=true  node=rabbit@rabbit3 queue_type=classic

#### Using quorum queues
# rabbitmqadmin declare queue name=answers-processing-command-queue \
#   node=rabbit@rabbit1 queue_type=quorum
# rabbitmqadmin declare queue name=answers-processing-event-queue \
#   node=rabbit@rabbit2 queue_type=quorum
# rabbitmqadmin declare queue name=answers-information-event-queue \
#   node=rabbit@rabbit3 queue_type=quorum

# rabbitmqadmin declare binding source=rabbit-example \
#   destination=answers-processing-command-queue destination_type=queue \
#   routing_key='command.*'
# rabbitmqadmin declare binding source=rabbit-example \
#   destination=answers-processing-event-queue destination_type=queue \
#   routing_key='event.processing.*'
# rabbitmqadmin declare binding source=rabbit-example \
#   destination=answers-information-event-queue destination_type=queue \
#   routing_key='event.info.*'
#
#### To enable Prometheus exported on the node (make sure that cluster name is unique)
# rabbitmq-plugins enable rabbitmq_prometheus
# firewall-cmd --permanent --add-port=15692/tcp
# firewall-cmd --reload
