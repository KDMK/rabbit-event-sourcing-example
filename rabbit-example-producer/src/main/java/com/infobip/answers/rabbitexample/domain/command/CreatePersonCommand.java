package com.infobip.answers.rabbitexample.domain.command;

public record CreatePersonCommand(String firstName,
                                  String lastName,
                                  String email) implements Command {

    @Override
    public String routingKey() {
        return "command.processing.person";
    }

}
