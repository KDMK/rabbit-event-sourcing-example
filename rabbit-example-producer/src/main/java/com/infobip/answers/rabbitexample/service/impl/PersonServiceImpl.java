package com.infobip.answers.rabbitexample.service.impl;

import java.time.Instant;
import java.util.List;

import com.infobip.answers.rabbitexample.domain.Person;
import com.infobip.answers.rabbitexample.repository.PersonRepository;
import com.infobip.answers.rabbitexample.service.PersonService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Mono<Person> create(Person person) {
        return Mono.just(personRepository.save(person));
    }

    @Override
    public Mono<List<Person>> getAll() {
        return Mono.just(personRepository.findAll());
    }

    @Override
    public Mono<Person> get(Long id) {
        return Mono.just(personRepository.getById(id));
    }

    @Override
    public Mono<Person> save(Person person) {
        return Mono.just(personRepository.save(person));
    }

    @Override
    public Mono<Person> delete(Long id) {
        var person = personRepository.getById(id);
        personRepository.delete(person);

        return Mono.just(person);
    }

}
