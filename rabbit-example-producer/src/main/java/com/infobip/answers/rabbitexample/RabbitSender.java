package com.infobip.answers.rabbitexample;

import com.infobip.answers.rabbitexample.domain.command.Command;
import com.infobip.answers.rabbitexample.domain.command.CreatePersonCommand;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RabbitSender {

    private final RabbitTemplate rabbitTemplate;

    public RabbitSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendCommand(Command command) {
        rabbitTemplate.convertAndSend("rabbit-example", command.routingKey(), command);
    }

    @Scheduled(fixedDelay = 10000, initialDelay = 10000)
    public void send() {
        System.out.println("Sending message...");
        var newPersonCommand = new CreatePersonCommand("John", "Doe", "johndoe@test.com");
        sendCommand(newPersonCommand);
    }

}
