package com.infobip.answers.rabbitexample.processing;

import java.time.Instant;

import com.infobip.answers.rabbitexample.domain.Person;
import com.infobip.answers.rabbitexample.domain.command.Command;
import com.infobip.answers.rabbitexample.domain.command.CreatePersonCommand;
import com.infobip.answers.rabbitexample.domain.command.DeletePersonCommand;
import com.infobip.answers.rabbitexample.domain.command.UpdatePersonCommand;
import com.infobip.answers.rabbitexample.domain.event.processing.DeletePersonProcessingEvent;
import com.infobip.answers.rabbitexample.domain.event.processing.NewPersonProcessingEvent;
import com.infobip.answers.rabbitexample.domain.event.processing.UpdatePersonProcessingEvent;
import com.infobip.answers.rabbitexample.domain.info.NewPersonInfoEvent;
import com.infobip.answers.rabbitexample.service.PersonService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RabbitListener(queues = Command.TARGET_QUEUE)
public class ProcessingCommandHandler {

    private final PersonService personService;
    private final RabbitTemplate rabbitTemplate;

    public ProcessingCommandHandler(PersonService personService, RabbitTemplate rabbitTemplate) {
        this.personService = personService;
        this.rabbitTemplate = rabbitTemplate;
    }

    private void handle(CreatePersonCommand event) {
        var person = new Person(null,
                                event.firstName(),
                                event.lastName(),
                                event.email(),
                                Instant.now().toEpochMilli(),
                                Instant.now().toEpochMilli());
        personService.save(person)
                     .subscribe(updatedPerson -> {
                         var infoEvent = new NewPersonInfoEvent(person.getFirstName(), person.getLastName(), person.getCreatedAt());
                         var processingEvent = new NewPersonProcessingEvent(person.getFirstName(),
                                                                            person.getLastName(),
                                                                            person.getEmail(),
                                                                            person.getCreatedAt());

                         rabbitTemplate.convertAndSend("rabbit-example", infoEvent.routingKey(), infoEvent);
                         rabbitTemplate.convertAndSend("rabbit-example", processingEvent.routingKey(), processingEvent);
                     });
    }

    private void handle(UpdatePersonCommand command) {
        personService.get(command.id())
                     .flatMap(person -> {
                         person.setFirstName(command.firstName());
                         person.setLastName(command.lastName());
                         person.setEmail(command.email());
                         person.setLastModified(Instant.now().toEpochMilli());

                         return personService.save(person);
                     }).subscribe(person -> {
                         var processingEvent = new UpdatePersonProcessingEvent(person.getId(),
                                                                               person.getFirstName(),
                                                                               person.getLastName(),
                                                                               person.getEmail(),
                                                                               person.getLastModified());

                         rabbitTemplate.convertAndSend("rabbit-example", processingEvent.routingKey(), processingEvent);
                     });

    }

    private void handle(DeletePersonCommand command) {
        personService.delete(command.id())
                     .subscribe(person -> {
                         var processingEvent = new DeletePersonProcessingEvent(person.getId(),
                                                                               person.getFirstName(),
                                                                               person.getLastName(),
                                                                               Instant.now().toEpochMilli());
                         rabbitTemplate.convertAndSend("rabbit-example", processingEvent.routingKey(), processingEvent);
                     });
    }

    @RabbitHandler
    @Transactional
    public void handleIncomingCommand(Command in) {
        switch (in) {
            case CreatePersonCommand command -> handle(command);
            case UpdatePersonCommand command -> handle(command);
            case DeletePersonCommand command -> handle(command);
            default -> System.out.println("Unsupported type");
        }
    }

}
