package com.infobip.answers.rabbitexample.service;

import java.util.List;

import com.infobip.answers.rabbitexample.domain.Person;
import reactor.core.publisher.Mono;

public interface PersonService {

    Mono<Person> create(Person person);

    Mono<List<Person>> getAll();

    Mono<Person> get(Long id);

    Mono<Person> save(Person person);

    Mono<Person> delete(Long id);

}
