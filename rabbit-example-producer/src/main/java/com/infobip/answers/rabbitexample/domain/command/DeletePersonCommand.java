package com.infobip.answers.rabbitexample.domain.command;

public record DeletePersonCommand(Long id) implements Command {

    @Override
    public String routingKey() {
        return "command.processing.person";
    }

}
