package com.infobip.answers.rabbitexample.web.dto;

public record UpdatePersonDto(Long id,
                              String firstName,
                              String lastName,
                              String email) {

}
