package com.infobip.answers.rabbitexample.domain.event.processing;

public interface ProcessingEvent {

    String TARGET_QUEUE = "answers-processing-event-queue";

    String routingKey();
}
