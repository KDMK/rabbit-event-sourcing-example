package com.infobip.answers.rabbitexample.web.dto;

public record CreatePersonDto(String firstName,
                              String lastName,
                              String email) {

}
