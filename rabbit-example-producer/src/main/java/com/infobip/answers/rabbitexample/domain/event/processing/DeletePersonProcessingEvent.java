package com.infobip.answers.rabbitexample.domain.event.processing;

public record DeletePersonProcessingEvent(Long id,
                                          String firstName,
                                          String lastName,
                                          Long deletionTimestamp) implements ProcessingEvent {

    @Override
    public String routingKey() {
        return "event.processing.person";
    }

}
