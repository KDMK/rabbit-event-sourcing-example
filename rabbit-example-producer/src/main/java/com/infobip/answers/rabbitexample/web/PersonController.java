package com.infobip.answers.rabbitexample.web;

import java.util.List;

import com.infobip.answers.rabbitexample.RabbitSender;
import com.infobip.answers.rabbitexample.domain.Person;
import com.infobip.answers.rabbitexample.domain.command.CreatePersonCommand;
import com.infobip.answers.rabbitexample.domain.command.UpdatePersonCommand;
import com.infobip.answers.rabbitexample.service.PersonService;
import com.infobip.answers.rabbitexample.web.dto.CreatePersonDto;
import com.infobip.answers.rabbitexample.web.dto.UpdatePersonDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    private final PersonService personService;
    private final RabbitSender rabbitSender;

    public PersonController(PersonService personService, RabbitSender rabbitSender) {
        this.personService = personService;
        this.rabbitSender = rabbitSender;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Mono<List<Person>> getAll() {
        return personService.getAll();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Person> createDirect(@RequestBody CreatePersonDto createPersonDto) {
        return personService.create(Person.from(createPersonDto));
    }

    @PostMapping("/create/enqueue")
    @ResponseStatus(HttpStatus.CREATED)
    public void createCommand(@RequestBody CreatePersonDto createPersonDto) {
        rabbitSender.sendCommand(new CreatePersonCommand(createPersonDto.firstName(),
                                                         createPersonDto.lastName(),
                                                         createPersonDto.email()));
    }

    @PostMapping("/update/enqueue")
    @ResponseStatus(HttpStatus.OK)
    public void updateCommand(@RequestBody UpdatePersonDto updatePersonDto) {
        rabbitSender.sendCommand(new UpdatePersonCommand(updatePersonDto.id(),
                                                         updatePersonDto.firstName(),
                                                         updatePersonDto.lastName(),
                                                         updatePersonDto.email()));
    }

}
