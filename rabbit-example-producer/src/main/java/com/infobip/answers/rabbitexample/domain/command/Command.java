package com.infobip.answers.rabbitexample.domain.command;

public interface Command {

    String TARGET_QUEUE = "answers-processing-command-queue";

    String routingKey();
}
