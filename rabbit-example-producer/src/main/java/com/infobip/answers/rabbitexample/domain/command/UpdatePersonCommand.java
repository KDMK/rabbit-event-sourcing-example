package com.infobip.answers.rabbitexample.domain.command;

public record UpdatePersonCommand(
    Long id,
    String firstName,
    String lastName,
    String email
) implements Command {

    @Override
    public String routingKey() {
        return "command.processing.person";
    }

}
