package com.infobip.answers.rabbitexample.repository;

import com.infobip.answers.rabbitexample.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {

}
