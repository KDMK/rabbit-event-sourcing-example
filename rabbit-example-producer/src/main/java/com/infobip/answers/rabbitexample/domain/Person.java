package com.infobip.answers.rabbitexample.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

import com.infobip.answers.rabbitexample.web.dto.CreatePersonDto;

@Entity
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    String email;

    @Column
    Long createdAt;

    @Column
    Long lastModified;

    public Person() {

    }

    public static Person from(CreatePersonDto createPersonDto) {
        return new Person(
            null,
            createPersonDto.firstName(),
            createPersonDto.lastName(),
            createPersonDto.email(),
            Instant.now().toEpochMilli(),
            Instant.now().toEpochMilli()
        );
    }

    public Person(Long id, String firstName, String lastName, String email, Long createdAt, Long lastModified) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.createdAt = createdAt;
        this.lastModified = lastModified;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

}
