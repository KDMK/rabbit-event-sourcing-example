package com.infobip.answers.rabbitexample.processing;

import com.infobip.answers.rabbitexample.domain.event.processing.DeletePersonProcessingEvent;
import com.infobip.answers.rabbitexample.domain.event.processing.NewPersonProcessingEvent;
import com.infobip.answers.rabbitexample.domain.event.processing.ProcessingEvent;
import com.infobip.answers.rabbitexample.domain.event.processing.UpdatePersonProcessingEvent;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@RabbitListener(queues = ProcessingEvent.TARGET_QUEUE)
public class ProcessingEventListener {

    private void handle(NewPersonProcessingEvent event) {
        System.out.printf("[HANDLER] Sending email to %s. Person created at timestamp %d%n", event.email(), event.timestamp());
    }

    private void handle(UpdatePersonProcessingEvent event) {
        System.out.printf("[HANDLER] Updated person %s %s at timestamp %d%n", event.firstName(), event.lastName(), event.timestamp());
    }

    private void handle(DeletePersonProcessingEvent event) {
        System.out.printf("[HANDLER] Deleting person %s %s at %d%n", event.firstName(), event.lastName(), event.deletionTimestamp());
    }

    @RabbitHandler
    public void handleIncomingEvent(ProcessingEvent event) {
        switch (event) {
            case NewPersonProcessingEvent e -> handle(e);
            case UpdatePersonProcessingEvent e -> handle(e);
            case DeletePersonProcessingEvent e -> handle(e);
            default -> System.out.println("Non processable");
        }
    }

}
