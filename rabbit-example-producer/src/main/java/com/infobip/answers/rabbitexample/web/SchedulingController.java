package com.infobip.answers.rabbitexample.web;

import com.infobip.answers.rabbitexample.RabbitSender;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SchedulingController {

    private final ScheduledAnnotationBeanPostProcessor postProcessor;

    private final RabbitSender rabbitSender;

    public SchedulingController(ScheduledAnnotationBeanPostProcessor postProcessor, RabbitSender rabbitSender) {
        this.postProcessor = postProcessor;
        this.rabbitSender = rabbitSender;
    }

    @PostMapping("/stop-scheduler")
    public void stopScheduling() {
        postProcessor.postProcessBeforeDestruction(rabbitSender, "rabbitSender");
    }

    @PostMapping("/start-scheduler")
    public void startScheduler() {
        postProcessor.postProcessAfterInitialization(rabbitSender, "rabbitSender");
    }

}
