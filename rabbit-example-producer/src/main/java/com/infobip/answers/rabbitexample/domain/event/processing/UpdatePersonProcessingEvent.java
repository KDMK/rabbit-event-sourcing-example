package com.infobip.answers.rabbitexample.domain.event.processing;

public record UpdatePersonProcessingEvent(Long id,
                                          String firstName,
                                          String lastName,
                                          String email,
                                          Long timestamp
) implements ProcessingEvent {

    @Override
    public String routingKey() {
        return "event.processing.person";
    }

}
