package com.infobip.answers.rabbitexample;

import com.infobip.answers.rabbitexample.domain.info.InformationEvent;
import com.infobip.answers.rabbitexample.domain.info.NewPersonInfoEvent;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = InformationEvent.TARGET_QUEUE)
public class InfoEventListener {

    private void handle(NewPersonInfoEvent event) {
        System.out.printf("Created new person %s %s at timestamp %d%n", event.firstName(), event.lastName(), event.creationTimestamp());
    }

    @RabbitHandler
    public void handleIncomingEvent(InformationEvent event) {
        switch (event) {
            case NewPersonInfoEvent e -> handle(e);
            default -> System.out.println("Non processable");
        }
    }

}
