package com.infobip.answers.rabbitexample.configuration;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.amqp.support.DefaultAmqpHeaderMapper;

@Configuration
public class RabbitConfiguration {

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public DefaultAmqpHeaderMapper inboundMapper() {
        var inboundMapper = DefaultAmqpHeaderMapper.inboundMapper();
        inboundMapper.setRequestHeaderNames("*");
        inboundMapper.setReplyHeaderNames("*");
        return inboundMapper;
    }

    @Bean
    public DefaultAmqpHeaderMapper outboundMapper() {
        var outboundMapper = DefaultAmqpHeaderMapper.outboundMapper();
        outboundMapper.setRequestHeaderNames("*");
        outboundMapper.setReplyHeaderNames("*");
        return outboundMapper;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
